# dockerfiles

Here are some Dockerfiles. These are built and uploaded into this the gitlab image repository (not docker hub) because I mostly them here on gitlab.com.


[![pipeline status](https://gitlab.com/smcphee/dockerfiles/badges/master/pipeline.svg)](https://gitlab.com/smcphee/dockerfiles/-/commits/master)

## latex

This was forked and extended from [https://gitlab.com/aergus/dockerfiles](gl) and therefore the "latex" Dockerfile
inherits that [CC0 licence](./LICENSE.txt). I've added some modifications to it in order to support my thesis-building
project.

The built container can be freely used by pulling `registry.gitlab.com/smcphee/dockerfiles/latex:stable`. You can also [browse the images](reg1).
It's a pretty large image because LaTeχ is already *huge*, and I also also install the nonfree-fonts package on top of this. I've reduced the size of the image somewhat by doing the following:

* The base image is `debian:stable-slim` not `debian:testing`
* Instead of `apt-get install texlive-full` (which is freakin' massive), it uses `apt-get install texlive-latex-extra latexmk texlive-xetex` (I use xetex so I install it).

This reduces the size of the image down to 500MB from 5GB!

[gl]: https://gitlab.com/aergus/dockerfiles

[reg1]: https://gitlab.com/smcphee/dockerfiles/container_registry/eyJuYW1lIjoic21jcGhlZS9kb2NrZXJmaWxlcy9sYXRleCIsInRhZ3NfcGF0aCI6Ii9zbWNwaGVlL2RvY2tlcmZpbGVzL3JlZ2lzdHJ5L3JlcG9zaXRvcnkvMTAxNjMwOC90YWdzP2Zvcm1hdD1qc29uIiwiaWQiOjEwMTYzMDh9

## corretto11

This is a general purpose Maven 3.6 + Amazon `corretto11` (open JDK) runtime which I use for *building* private projects. The base image is Amazon's Amazon Linux 2 based corretto11 image. It's got `make` and `git` and some other additional dependencies also installed in it.

The built container can be freely used by pulling `registry.gitlab.com/smcphee/dockerfiles/corretto11:stable`. You can also [browse the images](reg2).

[reg2]: https://gitlab.com/smcphee/dockerfiles/container_registry/eyJuYW1lIjoic21jcGhlZS9kb2NrZXJmaWxlcy9jb3JyZXR0bzExIiwidGFnc19wYXRoIjoiL3NtY3BoZWUvZG9ja2VyZmlsZXMvcmVnaXN0cnkvcmVwb3NpdG9yeS8xMDE2MzA5L3RhZ3M%2FZm9ybWF0PWpzb24iLCJpZCI6MTAxNjMwOX0=
